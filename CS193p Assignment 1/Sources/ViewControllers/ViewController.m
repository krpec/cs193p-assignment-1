#import "ViewController.h"
#import "PlayingCardDeck.h"
#import "PlayingCard.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;

@property (nonatomic, strong) Deck *deck;
@property (nonatomic) int flipCount;

@end

@implementation ViewController

@synthesize button, flipsLabel, deck, flipCount;

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (Deck *)deck {
    if (!deck) {
        deck = [[PlayingCardDeck alloc] init];
    }
    
    return deck;
}

- (void)setFlipCount:(int)aFlipCount {
    flipCount = aFlipCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips: %d", self.flipCount];
    NSLog(@"flipCount = %d", self.flipCount);
}

- (IBAction)buttonPushed:(UIButton *)sender {
    if ([sender.currentTitle length]) {
        [sender setBackgroundImage:[UIImage imageNamed:@"cardback"]
                          forState:UIControlStateNormal];
        [sender setTitle:@"" forState:UIControlStateNormal];
    }
    else {
        PlayingCard *card = (PlayingCard *)[self.deck drawRandomCard];
        
        if (!card) {
            [sender setBackgroundImage:nil forState:UIControlStateNormal];
            [sender setUserInteractionEnabled:NO];
        }
        else {
            if ([card.suit isEqualToString:@"♥︎"] ||
                [card.suit isEqualToString:@"♦︎"]) {
                [sender setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            }
            else {
                [sender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
            
            [sender setBackgroundImage:[UIImage imageNamed:@"cardfront"]
                              forState:UIControlStateNormal];
            [sender setTitle:[card contents]
                    forState:UIControlStateNormal];
        }
    }
    
    self.flipCount++;
}

@end
