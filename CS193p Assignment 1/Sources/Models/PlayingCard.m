#import "PlayingCard.h"

#pragma mark - Private interface
@interface PlayingCard ()

+ (NSArray *)rankStrings;

@end

#pragma mark - Implementation
@implementation PlayingCard

@synthesize suit, rank;

+ (NSArray *)validSuits {
    return @[@"♠︎", @"♣︎", @"♥︎", @"♦︎"];
}

+ (NSUInteger)maxRank {
    return [[PlayingCard rankStrings] count] - 1;
}

+ (NSArray *)rankStrings {
    return @[@"?", @"A", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"J", @"Q", @"K"];
}

- (NSString *)contents {
    NSArray *rankStrings = [PlayingCard rankStrings];
    return [rankStrings[self.rank] stringByAppendingString:self.suit];
}

#pragma mark - Setter and getter methods
- (void)setSuit:(NSString *)aSuit {
    if ([[PlayingCard validSuits] containsObject:aSuit]) {
        suit = aSuit;
    }
}

- (NSString *)suit {
    return suit ? suit : @"?";
}

- (void)setRank:(NSUInteger)aRank {
    if (aRank <= [PlayingCard maxRank]) {
        rank = aRank;
    }
}

@end
