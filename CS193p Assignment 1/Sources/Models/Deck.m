#import "Deck.h"

#pragma mark - Private interface
@interface Deck ()

@property (nonatomic, strong) NSMutableArray *cards;

@end


#pragma mark - Implementation
@implementation Deck

@synthesize cards;

- (NSMutableArray *)cards {
    if (!cards) {
        cards = [[NSMutableArray alloc] init];
    }
    
    return cards;
}

- (void)addCard:(Card *)card atTop:(BOOL)atTop {
    if (atTop) {
        [self.cards insertObject:card atIndex:0];
    }
    else {
        [self.cards addObject:card];
    }
}

- (void)addCard:(Card *)card {
    [self addCard:card atTop:NO];
}

- (Card *)drawRandomCard {
    Card *randomCard = nil;

    if ([self.cards count]) {
        unsigned index = arc4random() % [self.cards count];
        randomCard = self.cards[index];
        [self.cards removeObjectAtIndex:index];
    }
    
    return randomCard;
}

@end
